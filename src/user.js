// @ts-check

import React from 'react'
import Me from "./Images/me.jpg"

const user = {
  name: "Aaron",
  midname: "Lee",
  surname: "Marais",
  email: "its_me@aaronleem.co.za",
  short: "Junior Web Developer",

  born: {
    year: 1996, month: 11, day: 11
  },
  myPic: Me,
  location: {
    city: "Cape Town",
    country: "South Africa"
  },

  resume: [
    {
      title: "Information",
      boxes: {
        "Name": "FROM_USER:name+ +midname+ +surname",
        "Location": "FROM_USER:location.city+, +location.country",
        "Born": {
          value: "FROM_USER:born.day+ +born.month+ +born.year",
          action: string => {
            const [day, month, year] = string.split(" ").map(string => parseInt(string))
            const birthday = new Date(year, month - 1, day)
            const date = new Intl.DateTimeFormat('en-ZA', { year: "numeric", month: "long", day: "2-digit" })
            return date.format(birthday)
          }
        },
        "Age": {
          value: "FROM_USER:born.day+ +born.month+ +born.year",
          action: string => {
            const [day, month, year] = string.split(" ").map(string => parseInt(string))
            const birth = new Date(year, month - 1 , day)
            const age = new Date(Date.now() - birth.getTime())
            return Math.abs(age.getUTCFullYear() - 1970);
          }
        },
        "Phone": "+27 76 619 7593",
        "E-Mail": {
          value: "FROM_USER:email",
          action: string => <a href={"mailto:" + string}>{string}</a>
        },
        "Address": [
          "66 CJ Langenhoven street",
          <br key="1" />,
          "Parow North",
          <br key="2" />,
          "7500"
        ]
      }
    }, {
      title: "Skills",
      listOf: [
        "8+ years of experience with software and web development",
        "Ability to use user-centric development in combination with full-stack knowledge",
        "Well-versed in Python and JavaScript"
      ]
    }, {
      title: "Experience",
      experience: [
        {
          title: "Tenacious Digital",
          location: "Woodstock, Cape Town",
          start: {year: 2019, month: 10},
          duties: [
            "Retrofitting tests to products during agile development",
            "Finding and reporting on logical inconsistencies in code",
            "Assisting with the up-skill of intern employees"
          ]
        },
        {
          title: "Laptop and Game Console Technician",
          location: "DigiCafe, Cape Town",
          start: {year: 2015, month: 10},
          end: {year: 2016, month: 8},
          duties: [
            "I had to replace components such as laptop screens and console drives",
            "I had to replace components such as restistors and capacitors"
          ]
        },
        {
          title: "Storefront Salesperson",
          location: "DigiCafe, Cape Town",
          start: {year: 2014, month: 4},
          end: {year: 2016, month: 8},
          duties: [
            "Cleaning the store on open and close",
            "Making displays attractive",
            "Sorting new stock arriving from supplier",
            "Directing customers to appropriate items",
            "Training new salespeople"
          ]
        },
        {
          title: "Web Developer Job Shadowing",
          location: "Double-Eye, Cape Town",
          start: {year: 2010, month: 1},
          end: {year: 2011, month: 3},
          duties: [
            "Watching developers work on websites with multiple frameworks",
            "Assisting developers in choosing pathways for code",
            "Assisting developers in construction of websites"
          ]
        }
      ]
    }, {
      title: "Education",
      experience: [
        {
          title: "CoGrammar Software Engineer Bootcamp",
          location: "Salesian's Life Choices, Cape Town",
          start: {year: 2019, month: 4},
          end: {year: 2019, month: 10},
          duties: [
            "Making an iTunes API integration using React.JS and Express.js",
            "Making a game, Minecraft, using React.JS",
            "Creating this resume, on which I use React.JS"
          ]
        },
        {
          title: "National Certificate Vocational: Information Technology",
          location: "College of Cape Town, Crawford Campus",
          start: {year: 2013, month: 1},
          end: {year: 2017, month: 12},
          duties: [
            "Programming Fundamentals",
            "Systems Analysis",
            "Data Communition",
            "Networking"
          ]
        }
      ]
    }
  ]
}

export default user

export function parseUserResume(value) {
  if(Array.isArray(value)) return value

  let action
  if(typeof value !== "string") {
    action = value.action
    value = value.value
  }

  if(value.startsWith("FROM_USER:")) {
    value = value.slice(value.indexOf(':') + 1)

    let outputString = ""

    // eslint-disable-next-line
    for(const item of value.split("+")) {
      if(item.indexOf('.') !== -1) {
        const splitValues = item.split('.')

        if(splitValues[0] in user) {
          let currentItem = user,
            isCompleted = true

          // eslint-disable-next-line
          for(const index of splitValues) {
            if(Object.keys(currentItem).includes(index)) {
              currentItem = currentItem[index]
            } else {
              console.log(`Index "${index}" does not exist in path ` +
                    `"${item.slice(0, item.indexOf(index)-1)}"`)
              isCompleted = false
              break
            }
          }

          if(isCompleted) {
            outputString += currentItem
            continue
          }
        }
      }

      outputString += item in user ? user[item] : item
    }

    value = action ? action(outputString) : outputString
  }

  return value
}