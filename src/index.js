import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from "react-router-dom"

import { library } from '@fortawesome/fontawesome-svg-core'
import { faLocationArrow, faEnvelope, faHeart, faCoffee } from "@fortawesome/free-solid-svg-icons"
import { faGithub, faGitlab, faLinkedin, faTwitter } from "@fortawesome/free-brands-svg-icons"

library.add(faLocationArrow, faEnvelope, faHeart, faCoffee, faGithub, faGitlab, faLinkedin, faTwitter)

const Hydrator = process.env.NODE_ENV === "development" ? ReactDOM.render : ReactDOM.hydrate
Hydrator(
	<BrowserRouter>
		<App />
	</BrowserRouter>, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
