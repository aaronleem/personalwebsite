import React from "react"
import {Link} from "react-router-dom"
import Nav, {Collective} from "./style"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const links = [
    {
        "/": "Home",
        "/about": "About",
        "/projects": "Projects",
        "/resume": "Resume"
    }, {
        "https://github.com/gear4s/": <FontAwesomeIcon icon={["fab", "github"]} />,
        "https://gitlab.com/aaronleem/": <FontAwesomeIcon icon={["fab", "gitlab"]} />,
        "https://linkedin.com/in/aaronleem/": <FontAwesomeIcon icon={["fab", "linkedin"]} />
    }
]

export default props => (
	<Nav>
        {links.map((collectiveGroup, index) => (
            <Collective key={index}>
                {Object.entries(collectiveGroup).map(([path, title], index) => (
                    <div key={index} className="link">
                        {path.startsWith("/") ? (
                            <Link to={path}>{title}</Link>
                        ) : (
                            <a
                                target="_blank" rel="noopener noreferrer"
                                href={path}
                            >
                                {title}
                            </a>
                        )}
                    </div>
                ))}
            </Collective>
        ))}
	</Nav>
)
