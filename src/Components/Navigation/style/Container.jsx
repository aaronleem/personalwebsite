import styled, { css } from "styled-components/macro"

const ResponsiveFlexControl = css`
    @media screen and (max-width: 425px) {
        align-items: center;
        flex-direction: column;
    }
`

const PadAllButLastChild = css`
    .link:not(:last-child) {
        padding-right: 10px;
        margin-right: 10px;
    }
`

export default styled.nav`
    max-width: 100%;
    overflow: auto;
    padding: 15px 0;

    z-index: 1;

    display: flex;
    justify-content: space-between;

    ${ResponsiveFlexControl}
    ${PadAllButLastChild}
`
