import styled from "styled-components/macro"

export default styled.div`
    display: flex;

    &:not(:first-child) {
        font-size: 24px;
        
        @media screen and (max-width: 425px) {
            padding-top: 10px;
        }
    }
`
