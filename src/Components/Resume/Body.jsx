import React from 'react'
import { Box } from "./ContentContainers"

import user, { parseUserResume } from "../../user"

const { format: DateFormatter } = new Intl.DateTimeFormat('en-ZA', { year: "numeric", month: "long" })

const generateExperience = (experience, title) => (
    <Box.FullWidthInfoBox title={title}>
        {experience.map(({title, location, start, end=undefined, duties}, index) => {
            start = DateFormatter(new Date(start.year, start.month, 1))

            if(end)
                end = DateFormatter(new Date(end.year, end.month, 1))

            return (
                <div className="experience" key={index}>
                    <div className="title">{title}</div>
                    <div className="subtitle">{location} | {start} - {end || "Present"}</div>

                    <ul className="duties">
                        {duties.map((duty, index) => <li key={index}>{duty}</li>)}
                    </ul>
                </div>
            )
        })}
    </Box.FullWidthInfoBox>
)

export default props => (
    <React.Fragment>
        <div className="content">
            {user.resume.map(
                (
                    {
                        title, listOf, experience=undefined, content=undefined, boxes=undefined, items=undefined
                    },
                    index
                ) => (
                    <Box title={title} key={index}>
                        {content !== undefined ? (
                            <Box.FullWidthInfoBox title={title} content={content} />
                        ) : experience !== undefined ? (
                            generateExperience(experience, title)
                        ) : listOf !== undefined ? (
                            <Box.FullWidthInfoBox title={title}>
                                <ul>
                                    {listOf.map((item, index) => <li key={index}>{item}</li>)}
                                </ul>
                            </Box.FullWidthInfoBox>
                        ) : boxes !== undefined ? (
                            Object.entries(boxes).map(([title, value], index) => (
                                <Box.InfoBox title={title} key={index}
                                    content={parseUserResume.call(this, value)} />
                            ))
                        ) : items !== undefined ? (
                            items.map(({title, value}, index) => (
                                <Box.FullWidthInfoBox title={title} key={index}
                                    content={parseUserResume.call(this, value)} />
                            ))
                        ) : null}
                    </Box>
                )
            )}
        </div>
    </React.Fragment>
)
