import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { Box as BoxStyling } from "./style/Resume"

export const Box = React.forwardRef(
    ({className=undefined, title, children}, ref) => (
        <BoxStyling className={(className || "") + "box"} ref={ref}>
            {title ? <span className="title">{title}</span> : null}

            <div className="content">
                {children}
            </div>
        </BoxStyling>
    )
)

Box.InfoBox = ({title, content=undefined, children=undefined}) => (
    <Box className="info" title={title}>
        {content||children}
    </Box>
)

Box.FullWidthInfoBox = ({title, content=undefined, children=undefined}) => (
    <Box className="fw-info">
        {content||children}
    </Box>
)

export const Personal = props => (
    <div className="fullname">
        <span className="name">{props.first}</span>
        <span className="surname">{props.last}</span>

        <div className="short">
            {props.shortInfo}
        </div>
    </div>
)

export const InfoItem = ({icon, text=undefined, children=undefined}) => (
    <div className={"infoitem" + (!(text||children) ? " nocontent" : "")}>
        <FontAwesomeIcon icon={icon} />
        {children||text}
    </div>
)

InfoItem.Link = ({link, icon: ic, text:tx=undefined, children:cd=undefined}) => (
    <a href={link}>
        <InfoItem icon={ic} text={tx} children={cd} />
    </a>
)
