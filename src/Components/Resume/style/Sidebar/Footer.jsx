import { css } from 'styled-components'

export default css`
    footer {
        display: flex;
        justify-content: center;
        padding-top: 30px;
        font-size: 12.2px;

        svg {
            padding: 0 .3vw;
            transition: color .3s ease;
        }

        .fa-coffee:hover {
            color: brown;
        }

        .fa-heart:hover {
            color: hotpink;
        }
    }
`
