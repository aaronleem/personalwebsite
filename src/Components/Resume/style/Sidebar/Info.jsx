import { css } from 'styled-components'

export default css`
    .info {
        width: 280px;
        display: flex;
        flex-direction: column;
        align-items: center;
        font-size: 20px;
        
        .fullname {
            display: flex;
            align-items: flex-end;
            flex-direction: column;
            width: 100%;
        }

        .name {
            font-size: 45px;
            transform: translate(-11%, 60%) rotate(5.5deg);
            color: grey;
        }

        .surname {
            z-index: 1;
            font-size: 56px;
        }

        .short {
            align-self: center;
        }
    }

    .infoitem {
        display: flex;
        align-items: center;
        
        &:not(.nocontent) svg {
            color: grey;
            transform: scale(0.8);
            padding-right: 5px;
        }
    }
`