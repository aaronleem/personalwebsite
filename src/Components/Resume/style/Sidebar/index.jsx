import styled from 'styled-components'
import Info from "./Info"
import Footer from "./Footer"

export default styled.div`
    padding: 0 15px;
    display: flex;
    flex-direction: column;
    align-items: center;
  
    .pic {
        height: 275px;
        width: 275px;
        overflow: hidden;
        display: flex;
        align-items: center;
        justify-content: space-around;
        border-radius: 100%;
    }
  
    .pic img {
        height: 100%;
    }
  
    .leftbarEnd {
        display: flex;
        flex-direction: column;
    }

    .social {
        width: 75%;
        display: flex;
        justify-content: space-between;
        padding-top: 35px;
        
        svg {
            transition: color .5s ease;
            transform: scale(1.5);
        }
    }

    .fa-twitter:hover {
        color: #38A1F3;
    }

    .fa-linkedin:hover {
        color: #0077B5;
    }

    .fa-gitlab:hover {
        color: #e24329;
    }
    
    ${Info}
    ${Footer}
`
