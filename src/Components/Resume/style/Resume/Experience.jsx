import { css } from 'styled-components'

export default css`  
    .experience {
        padding-left: 15px;
        margin-left: 5px;
        position: relative;
        
        &:before {
            position: absolute;
            left: 0;
            top: 0;
            height: 105%;
            width: 5px;
            content: "";
            background-color: lightgray;
        }

        &:not(:first-child) {
            margin-top: 15px;
        }

        > .title {
            font-size: 22px;
        }

        > .subtitle {
            font-size: 18px;
            font-style: italic;
        }

        > .duties li {
            margin-top: 7px;
        }
    }
`