import styled from 'styled-components'
import Experience from "./Experience"

export { default as Box } from "./Box"

export default styled.div`
	display: flex;
	max-height: 100%;
 	color: rgb(240,240,240);

    & > .content {
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
        flex: 1;
        overflow: auto;
        max-height: calc(100% - (18px * 2 + 18px));
    
        position: absolute;
        padding-top: calc(18px * 2 + 18px);
        top: 0;
    
        border-left: 2px solid rgb(64, 64, 64);
    
        max-width: calc(95% - 280px - 30px);
        right: 0;
    }
  
    ::-webkit-scrollbar {
        width: 0px;
        background: transparent;
    }
  
    ul {
        margin: 0;
        padding-left: 17.5px;
    }

    ${Experience}
`