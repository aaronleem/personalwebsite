import styled from 'styled-components'
import InfoBox from "./InfoBox"

export default styled.div`
    padding-bottom: 15px;

    &.box {
        > .title {
            padding: 0 8px;
            font-size: 36px;
            font-family: 'Nunito', sans-serif;
        }

        > .content {
            display: flex;
            flex-wrap: wrap;
            background: rgb(0,0,0);
            background: linear-gradient(
                90deg,
                rgba(0,0,0,0.02) 0%,
                rgba(0,0,0,0.06) 30%,
                rgba(0,0,0,0.1) 100%);
        }
    }

    ${InfoBox}
`