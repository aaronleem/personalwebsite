import { css } from 'styled-components'

const TitleAndContentStyling = css`
    > .title {
        font-size: 24px;
        padding: 3px;
    }

    > .content {
        margin-top: 2px;
        padding: 2px 3px 3px 3px;
    }
`

export default css`
    &.infobox {
        padding: 15px 0;
        width: calc(50% - 7.5px);
        
        &:nth-child(2n+1) {
            padding-left: 4px;
            padding-right: 2px;
        }
        
        &:nth-child(2n+2) {
            padding-right: 4px;
            padding-left: 2px;
        }

        ${TitleAndContentStyling}
    }
    
    &.fw-infobox {
        padding: 15px 8px;

        ${TitleAndContentStyling}
    }
`