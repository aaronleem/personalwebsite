import React from 'react'

import { Personal, InfoItem } from './ContentContainers'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Sidebar from "./style/Sidebar"

import user from "../../user"

export default props => (
    <Sidebar className="about">
        <div className="pic">
            <img src={user.myPic} alt="Me" />
        </div>

        <div className="leftbarEnd">
            <div className="info">
                <Personal first={user.name} last={user.surname} shortInfo={user.short} />

                <InfoItem icon="location-arrow" text={user.location.city} />
                <InfoItem icon="envelope"><a href={"mailto:" + user.email}>E-Mail me</a></InfoItem>

                <div className="social">
                    <InfoItem.Link link="https://github.com/gear4s" icon={["fab", "github"]} />
                    <InfoItem.Link link="https://gitlab.com/aaronleem" icon={["fab", "gitlab"]} />
                    <InfoItem.Link link="https://linkedin.com/in/aaronleem" icon={["fab", "linkedin"]} />
                    <InfoItem.Link link="https://twitter.com/DaddyGruul" icon={["fab", "twitter"]} />
                </div>
            </div>

            <footer>
                Made with <FontAwesomeIcon icon="heart" /> and <FontAwesomeIcon icon="coffee" /> using React.JS
            </footer>
        </div>
    </Sidebar>
)
