// @ts-check

import React from 'react'
import Body from "./Body"
import Bar from "./Bar"
import Helmet from 'react-helmet'
import OverwriteBodyStyling from "./OverwriteBodyStyling"

import Resume from "./style/Resume"

export default () => (
	<Resume>
		<Helmet>
			<title>Resume</title>
			<link rel="canonical" href="http://aaronleem.co.za/resume" />
			<meta name="description" content="About Aaron Marais" />
		</Helmet>

		<OverwriteBodyStyling />

		<Bar />
		<Body />
	</Resume>
)
