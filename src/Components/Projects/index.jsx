import React from "react"
import {Helmet} from "react-helmet"

import Container, * as Projects from "./style"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ProjectsFile from "../../projects.js"

export default props => (
	<Container>
        <Helmet>
        	<title>Projects</title>
            <link rel="canonical" href="http://aaronleem.co.za/projects" />
            <meta name="description" content="Aaron Marais' list of projects" />
        </Helmet>
        
		{ProjectsFile.map((project, index) => (
			<Projects.Project key={index} src={project.image}>
				<div className="bgOverlay" />
				<div className="content">
					<div className="proper">
						<div className="title">
							{project.name}
						</div>

						<div className="description">
							{project.desc}
						</div>
					</div>

					<div className="social">
						<a target="_blank" rel="noopener noreferrer" href={project.github}>
							<FontAwesomeIcon icon={["fab", "github"]} />
						</a>

						<a target="_blank" rel="noopener noreferrer" href={project.demo}>
							<FontAwesomeIcon icon="external-link-alt" />
						</a>
					</div>
				</div>
			</Projects.Project>
		))}
	</Container>
)
