// @ts-check

import styled, {css} from "styled-components/macro"
import Responsiveness from "./Responsiveness"
import Content from "./Content"

const Overlay = css`
	.bgOverlay {
		border-radius: 25px;
		position: absolute;
		z-index: 2;
		background-color: rgba(0,0,0,.7);
		top: 0%;
		bottom: 0%;
		right: 0%;
		left: 0%;
	}
`

export default styled.div.attrs(props => ({
	style: {
		backgroundImage: `url("${props.src}")`
	}
}))`
	width: 320px;
	height: 220px;
	padding: 15px;
	margin: 25px;
	border-radius: 25px;
	background-size: auto 101%;
	background-position: center;
	z-index: 1;
	position: relative;

	transition: all .8s ease-in-out;

	${Content}
	${Overlay}
	${Responsiveness}
`
