// @ts-check
import { css } from 'styled-components'

const ProperContent = css`
    .proper {
        display: flex;
        flex-direction: column;
        align-items: center;

        .title {
            font-size: 36px;
        }

        .description {
            font-size: 24px;
        }
    }
`

const SocialContent = css`
    .social {
        display: flex;
        justify-content: space-between;
        width: 150px;
        font-size: 36px;
    }
`

export default /* Content */ css`
	.content {
		display: flex;
		flex-direction: column;
		position: absolute;
		align-items: center;
		justify-content: space-between;

		width: calc(100% - 15px);
		height: calc(100% - 15px);

		top: calc(15px / 2);
		left: calc(15px / 2);
		z-index: 3;

        ${ProperContent}
        ${SocialContent}
    }
`