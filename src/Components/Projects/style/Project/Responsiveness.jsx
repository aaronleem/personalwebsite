import { css } from "styled-components/macro"

export default css`
	/* Change sizing based on screen width */
    @media screen and (max-width: 768px) {
		width: 640px;
		height: 360px;

		&:first-child {
			margin-top: 0;
		}
    }
	
    @media screen and (max-width: 425px) {
		width: 325px;
		height: 200px;
    }
	
    @media screen and (max-width: 375px) {
		width: 295px;
		height: 190px;
    }
	
    @media screen and (max-width: 320px) {
		width: 240px;
		height: 150px;

		.proper {
			.title {
				font-size: 28px;
			}

			.description {
				font-size: 18px;
			}
		}

		.social {
			width: 90px;
			font-size: 28px;
		}
    }

    @media screen and (max-width: 425px) {
		margin: 15px 0;
    }

	/*
		Mobile vs Browser: Go!
		*/

	/* Mobile: Content always-on */
    @media (hover: none), (pointer: coarse) {
		.bgOverlay, .content {
			opacity: 1;
		}
	}
	
	/* Desktop *or* any pointer can hover: Content displayed on hover */
	@media (any-hover: hover), (pointer: fine) {
		.bgOverlay, .content {
			opacity: 0;
			transition: opacity .8s ease-in-out;
		}

		&:hover {
			.bgOverlay, .content {
				opacity: 1;
			}

			transform: scale(1.06);
		}
	}

`
