import styled from "styled-components/macro"

export default styled.div`
	flex: 1;
	padding-bottom: 20px;
`
