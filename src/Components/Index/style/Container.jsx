import styled, { css } from "styled-components/macro"

const backgroundImage = css`
	content: "";
	background-image: url(${props => props.src});
	position: absolute;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
	opacity: .05;
	pointer-events: none;
`

export default styled.div`
	display: flex;
	flex-direction: column;
	padding: 0 5%;
	min-height: 100vh;
	color: rgb(255,255,255);

	position: relative;

	&:before {
		${backgroundImage}
	}
`
