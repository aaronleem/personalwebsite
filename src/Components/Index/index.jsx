import React from "react"
import {Helmet} from "react-helmet"
import Index, {Body} from "./style"

import Navigation from "../Navigation"

import { withRouter } from "react-router"
import { Route, Switch } from "react-router-dom"

import Background from "../../Images/Background.svg"

import Landing from "../Landing"
import About from "../About"
import Projects from "../Projects"
import Resume from "../Resume"

export default withRouter(props => 
	<Index src={Background}>
		<Helmet defaultTitle="Aaron Lee Marais" titleTemplate="%s | Aaron Lee Marais">
			<link rel="canonical" href="http://aaronleem.co.za/" />
			
			<meta charSet="utf-8" />
			<meta name="theme-color" content="#2C2F33" />
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<meta name="description" content="Aaron Marais' personal website" />
		</Helmet>

		<Navigation />

		<Body className="body">
			<Switch>
				<Route path="/contact" component={() => <span>Hey uwu</span>} />
				<Route path="/resume" component={Resume} />
				<Route path="/projects" component={Projects} />
				<Route path="/about" component={About} />
				<Route path="/" component={Landing} />
			</Switch>
		</Body>
	</Index>
)
