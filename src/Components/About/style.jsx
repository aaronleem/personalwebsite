import styled from "styled-components/macro"

const AboutStyle = styled.div`
	display: flex;
	flex-direction: column;

    @media screen and (max-width: 425px) {
    	border-top: 1px solid rgba(0,0,0,.5);
    }

	&:not(:first-child) {
		margin-top: 15px;
	}
`

AboutStyle.Title = styled.div`
	font-size: 40px;
	align-self: center;
	padding: 10px;

    @media screen and (max-width: 425px) {
	    font-size: 32px;
	    text-align: center;
    }

    @media screen and (max-width: 375px) {
	    font-size: 28px;
	    text-align: center;
    }
`

AboutStyle.Content = styled.div`
    @media screen and (max-width: 375px) {
    	font-size: 18px;
    }
`

export default AboutStyle
