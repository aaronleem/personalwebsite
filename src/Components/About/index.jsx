import React from "react"
import {Helmet} from "react-helmet"

import About from "./style"

export default function AboutPage(props) {
	return (
		<React.Fragment>
            <Helmet>
            	<title>About</title>
                <link rel="canonical" href="http://aaronleem.co.za/about" />
                <meta name="description" content="About Aaron Marais" />
            </Helmet>
            
			<About>
				<About.Title>
					Who is Aaron?
				</About.Title>

				<About.Content>
					A 22-year old aspiring Full Stack Web Developer, Aaron loves
					getting to work, and help wherever he can.<br /><br />
					
					Always taking on new challenges, he never backs down and
					gives his all.
				</About.Content>
			</About>

			<About>
				<About.Title>
					Alright.. So what does he like doing?
				</About.Title>

				<About.Content>
					He mostly enjoys playing with his two little pups, Jackie and Shortie.
					<br /><br />
					He's an ex-photographer, and has done work for the City of Cape Town,
					mostly during the 2010 FIFA World Cup, in which he was an events
					photographer at locations which the World Cup Games were broadcast
					onto open fields. Since the World Cup, he's enjoyed doing nature
					and personal photography, with his father doing videography.
				</About.Content>
			</About>

			<About>
				<About.Title>
					I meant with computers..
				</About.Title>

				<About.Content>
					Oh, he enjoys playing different types of games, like World of Warcraft,
					which offers him the opportunity to clear his mind and start a new project
					with a fresh way of thinking.
					<br /><br />
					He also enjoys taking apart his computer, setting up Internet of
					Things devices (such as his Raspberry Pi entertainment system),
					and making sure his home network is up to date with the latest
					of technologies.
				</About.Content>
			</About>

			<About>
				<About.Title>
					I mean programming!
				</About.Title>

				<About.Content>
					<span style={{fontStyle: "italic"}}>Ohhh</span>, he enjoys dabbling
					in such things as Node.JS, React, Angular, Python, Java... Basically
					a whole host of languages, frameworks and libraries that make it
					much easier to create what he needs.
					<br /><br />
					He often helps in various support channels such as the <a href="https://pythondiscord.com/">Python Discord</a>
					&nbsp;server, the <a href="https://wiki.ubuntu.com/IRC/ChannelList">#ubuntu</a>, <a href="https://wiki.debian.org/IRC">#debian</a> and <a href="https://wiki.centos.org/irc">#centos</a> channels on the
					Freenode IRC network.
				</About.Content>
			</About>
		</React.Fragment>
	)
}
