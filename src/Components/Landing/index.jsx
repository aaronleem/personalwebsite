import React from "react"

import Landing from "./style"

export default props => (
	<Landing>
		<div className="welcome">
			Aaron's website...
		</div>

		<div className="blurb">
			Very interesting...
		</div>
	</Landing>
)
