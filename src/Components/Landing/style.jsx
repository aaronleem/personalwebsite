import styled from "styled-components/macro"

export default styled.div`
	overflow: hidden;

	height: 100%;

	position: absolute;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;

	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	.welcome {
	    font-weight: 700;
	    font-size: 42px;
	    line-height: 44px;
		padding-bottom: 15px;

	    @media screen and (max-width: 320px) {
		    font-size: 36px;
	    }
	}

	.blurb {
		font-style: italic;
	}
`
