import Index from "./Components/Index/"

import { library } from '@fortawesome/fontawesome-svg-core'
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons/faExternalLinkAlt'
import { faGithub } from '@fortawesome/free-brands-svg-icons/faGithub'
import { faGitlab } from '@fortawesome/free-brands-svg-icons/faGitlab'
import { faLinkedin } from '@fortawesome/free-brands-svg-icons/faLinkedin'

library.add(
	faGithub, faGitlab, faLinkedin, faExternalLinkAlt
)

export default Index
