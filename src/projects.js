import WonImage from "./Images/Weather or Not.png"
import MsImage from "./Images/Minesweeper.png"
import CalImage from "./Images/Calculator.png"
import PlcImage from "./Images/Play Converter.png"
import TodImage from "./Images/ToDo.png"
import SopImage from "./Images/Slice of Apple.png"

export default [
	{
		name: "Weather or Not",
		desc: "The weather is hot",
		image: WonImage,
		demo: "https://projects.aaronleem.co.za/school/weather/",
		github: "https://github.com/aaron-marais-tasks/task-12"
	},
	{
		name: "Minesweeper",
		desc: "A simple game",
		image: MsImage,
		demo: "https://projects.aaronleem.co.za/school/minesweeper/",
		github: "https://github.com/aaron-marais-tasks/task-10"
	},
	{
		name: "Calculator",
		desc: "Calculated move",
		image: CalImage,
		demo: "https://projects.aaronleem.co.za/school/calculator/",
		github: "https://github.com/aaron-marais-tasks/Task-8/tree/Compulsory-2"
	},
	{
		name: "Play Convert",
		desc: "Cash to Cards",
		image: PlcImage,
		demo: "https://projects.aaronleem.co.za/school/play_convert/",
		github: "https://github.com/aaron-marais-tasks/Task-9/"
	},
	{
		name: "To-do",
		desc: "What to do..",
		image: TodImage,
		demo: "https://projects.aaronleem.co.za/school/todo/",
		github: "https://github.com/aaron-marais-tasks/Task-8/tree/Compulsory-3"
	},
	{
		name: "Slice of Apple",
		desc: "Want a piece?",
		image: SopImage,
		demo: "https://projects.aaronleem.co.za/school/itunes/",
		github: "https://github.com/aaron-marais-tasks/Task-19"
	}
]
