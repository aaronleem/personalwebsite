import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import Bar from "./Components/Resume/Bar"

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  ReactDOM.unmountComponentAtNode(container);
  container.remove();
  container = null;
});

jest.mock("./user", () => ({
  name: "Aaron",
  midname: "Lee",
  surname: "Marais",
  email: "its_me@aaronleem.co.za",
  short: "Junior Web Developer",

  born: {
    year: 1996, month: 11, day: 11
  },
  location: {
    city: "Cape Town",
    country: "South Africa"
  }
}))

it('renders without crashing', () => {
  ReactDOM.render(<Bar />, container);

  expect(container.querySelector(".infoitem").innerHTML).toBe("Cape Town")
});
