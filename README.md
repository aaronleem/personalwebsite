This is the source for the website of [Aaron Lee Marais](https://aaronleem.co.za/)

Download and Installing:
```bash
$ git clone https://gitlab.com/aaronleem/personalwebsite.git AaronMaraisSite
$ cd ./AaronMaraisSite/
$ npm install
```

Running:
```bash
$ npm run start
```

Building:
```bash
$ npm run build
```

Server-side rendering:
```bash
$ node server/bootstrap.js
```
