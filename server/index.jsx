import path from 'path'
import fs from 'fs'

import express from 'express'
import React from 'react'
import Helmet from 'react-helmet'
import ReactDOMServer from 'react-dom/server'
import { ServerStyleSheet } from 'styled-components'
import { StaticRouter } from "react-router-dom"

import registerRequireContextHook from 'babel-plugin-require-context-hook/register'
registerRequireContextHook()

import App from '../src/App'

const PORT = 8080
const app = express()

const router = express.Router()

const serverRenderer = (req, res, next) => {
  const filePath = path.resolve(__dirname, '..', 'build', 'index.html')
  const context = {}

  fs.readFile(filePath, 'utf8', (err, htmlData) => {
      if(err) {
          console.error('err', err)
          return res.status(404).end()
      }

      const sheet = new ServerStyleSheet()
      const html = ReactDOMServer.renderToString(sheet.collectStyles(
          <StaticRouter location={req.baseUrl} context={context}>
              <App />
          </StaticRouter>
      ))
      const styleTags = sheet.getStyleTags()
      sheet.seal()

      if (context.url)
          return res.redirect(context.url)

      let helmet = null
      {
        const helmetStatic = Helmet.renderStatic()
        const helmetEntries = Object.entries(helmetStatic)
        helmetEntries.forEach(item => {
          item[1] = item[1].toString()
        })

        helmet = helmetEntries
          .filter(item => item[1] !== "")
          .map(item => item[1])
      }

      res.send(
          htmlData.replace(
            '</head>',
            `${styleTags}${helmet.join("")}</head>`
          ).replace(
              '<div id="root"></div>',
              `<div id="root">${html}</div>`
          )
      )
  })
}

router.use('^/$', serverRenderer)
router.use(express.static(path.resolve(__dirname, '..', 'build'), {maxAge: '30d'}))
router.use('*', serverRenderer)

app.use(router)

app.listen(PORT, (error) => {
    if(error)
        return console.log(error)

    console.log(`Listening on ${PORT}`)
})
