require('ignore-styles')

require('@babel/register')({
  	ignore: [/(node_modules)/],
  	presets: ['@babel/preset-env', '@babel/preset-react', ],
  	plugins: [
  		"macros", "require-context-hook",
		[
  			"file-loader",
    		{
		        "name": "[name].[md5:hash:hex:8].[ext]",
		        "extensions": ["svg", "png", "jpg"],
		        "publicPath": "/static/media",
		        "outputPath": "/../../build/static/media",
		        "context": "",
		        "limit": 10000
    		}
    	]
	]
})

require('./index.jsx')
